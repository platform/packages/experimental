package com.google.android.hiddenapi.testapp;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;

public class NoChecksActivity extends Activity {

  public static String TAG = "compat-test";

  private TextView mTextView;
  private final StringBuilder mText = new StringBuilder();
  private final Runnable mUpdateRunnable = () -> mTextView.setText(mText);

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_nochecks);
    mTextView = (TextView) findViewById(R.id.textView);
  }

  @Override
  public void onStart() {
    super.onStart();
    try {
      Process am = Runtime.getRuntime().exec(
          "am instrument --no-hidden-api-checks mypackage/.MainInstrumentation");
      new ReaderThread(am.getErrorStream()).start();
      new ReaderThread(am.getInputStream()).start();
    } catch (IOException ioe) {
      throw new RuntimeException(ioe);
    }
    // TODO get proc output, display in text box
  }

  public synchronized void logLine(String line) {
    if (line == null) {
      return;
    }
    Log.d(TAG, line);
    mText.append(line);
    mTextView.post(mUpdateRunnable);
  }

  class ReaderThread extends Thread {
    private final BufferedReader mInput;
    public ReaderThread(InputStream in) {
      mInput = new BufferedReader(new InputStreamReader(in));
    }

    public void run() {
      try {
        String line;
        do {
          line = mInput.readLine();
          logLine(line);
        } while (line != null);
      } catch (IOException e) {
        Log.e(TAG, "ReaderThread threw", e);
      }
    }
  }

}
