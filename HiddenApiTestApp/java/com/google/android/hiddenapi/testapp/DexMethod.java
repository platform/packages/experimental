package com.google.android.hiddenapi.testapp;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class DexMethod extends DexMember {
  private final List<String> mParamTypeList;

  public DexMethod(String className, String name, String signature) {
      super(className, name, parseDexReturnType(signature));
      mParamTypeList = parseDexTypeList(signature);
  }

  public String getDexSignature() {
      return "(" + String.join("", mParamTypeList) + ")" + getDexType();
  }

  public List<String> getJavaParameterTypes() {
      return mParamTypeList.stream().map(DexMember::dexToJavaType).collect(Collectors.toList());
  }

  public boolean isConstructor() {
      return "<init>".equals(getName()) && "V".equals(getDexType());
  }

  public boolean isStaticConstructor() {
      return "<clinit>".equals(getName()) && "V".equals(getDexType());
  }

  @Override
  public String toString() {
      return getJavaType() + " " + getJavaClassName() + "." + getName()
              + "(" + String.join(", ", getJavaParameterTypes()) + ")";
  }

  public static Class<?> getTypeClass(String type) throws Exception {
    switch (type) {
      case "V":
        return Void.TYPE;
      case "Z":
        return Boolean.TYPE;
      case "B":
        return Byte.TYPE;
      case "C":
        return Character.TYPE;
      case "S":
        return Short.TYPE;
      case "I":
        return Integer.TYPE;
      case "J":
        return Long.TYPE;
      case "F":
        return Float.TYPE;
      case "D":
        return Double.TYPE;
      default:
        if (type.startsWith("L")) {
          return Class.forName(dexToJavaType(type));
        } else {
          throw new IllegalArgumentException("Unknown type: " + type);
        }

    }
  }

  public Class<?>[] getParameterTypes() throws Exception {
    Class<?>[] types = new Class<?>[mParamTypeList.size()];
    for (int i = 0; i < mParamTypeList.size(); ++i) {
      String type = mParamTypeList.get(i);
      types[i] = getTypeClass(type);
    }
    return types;
  }

  private static Matcher matchSignature(String signature) {
      Matcher m = Pattern.compile("^\\((.*)\\)(.*)$").matcher(signature);
      if (!m.matches()) {
          throw new RuntimeException("Could not parse method signature: " + signature);
      }
      return m;
  }

  private static String parseDexReturnType(String signature) {
      return matchSignature(signature).group(2);
  }

  private static List<String> parseDexTypeList(String signature) {
      String typeSequence = matchSignature(signature).group(1);
      List<String> list = new ArrayList<String>();
      while (!typeSequence.isEmpty()) {
          String type = firstDexTypeFromList(typeSequence);
          list.add(type);
          typeSequence = typeSequence.substring(type.length());
      }
      return list;
  }

  /**
   * Returns the first dex type in `typeList` or throws a ParserException
   * if a dex type is not recognized. The input is not changed.
   */
  private static String firstDexTypeFromList(String typeList) {
      String dexDimension = "";
      while (typeList.startsWith("[")) {
          dexDimension += "[";
          typeList = typeList.substring(1);
      }

      String type = null;
      if (typeList.startsWith("V")
              || typeList.startsWith("Z")
              || typeList.startsWith("B")
              || typeList.startsWith("C")
              || typeList.startsWith("S")
              || typeList.startsWith("I")
              || typeList.startsWith("J")
              || typeList.startsWith("F")
              || typeList.startsWith("D")) {
          type = typeList.substring(0, 1);
      } else if (typeList.startsWith("L") && typeList.indexOf(";") > 0) {
          type = typeList.substring(0, typeList.indexOf(";") + 1);
      } else {
          throw new RuntimeException("Unexpected dex type in \"" + typeList + "\"");
      }

      return dexDimension + type;
  }
}
