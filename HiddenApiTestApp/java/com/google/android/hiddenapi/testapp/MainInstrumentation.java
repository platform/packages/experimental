package com.google.android.hiddenapi.testapp;

import android.app.Instrumentation;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;

public class MainInstrumentation extends Instrumentation {

  private static final String TAG = "compat-test";

  @Override
  public void onCreate(Bundle args) {
    super.onCreate(args);
    Log.d(TAG, "MainInstrumentation.onCreate()");
    start();
  }

  @Override
  public void onStart() {
    Log.d(TAG, "MainInstrumentation.onStart()");
    Intent i = new Intent(getTargetContext(), MainActivity.class);
    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    getContext().startActivity(i);
    Log.d(TAG, "Started Activity");
  }
}
