package com.google.android.hiddenapi.testapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Executable;
import java.lang.reflect.Field;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ReflectReceiver extends BroadcastReceiver {

  public static final String EXTRA_CLASS = "class";
  public static final String EXTRA_SIGNATURE = "signature";

  private static final Pattern REGEX_CLASS = Pattern.compile("^L[^->]*;$");
  private static final Pattern REGEX_FIELD = Pattern.compile("^(L[^->]*;)->(.*):(.*)$");
  private static final Pattern REGEX_METHOD = Pattern.compile("^(L[^->]*;)->(.*)(\\(.*\\).*)$");

  public ReflectReceiver() {
    try {
      lookupSignature(
          "Landroid/bluetooth/BluetoothAdapter;->setDiscoverableTimeout(I)V");
      Log.d(getClass().getSimpleName(), "Successfully accessed method!");
    } catch (Exception e) {
      Log.e(getClass().getSimpleName(), "Failed", e);
    }
  }

  private void lookupClass(String className) throws Exception {
    Class<?> clazz= Class.forName(className);
    setResult(0, "Got class: " + clazz, null);
  }

  private void lookupSignature(String signature) throws Exception {
    Matcher matchClass = REGEX_CLASS.matcher(signature);
    Matcher matchField = REGEX_FIELD.matcher(signature);
    Matcher matchMethod = REGEX_METHOD.matcher(signature);
    int matchCount = (matchClass.matches() ? 1 : 0) + (matchField.matches() ? 1 : 0) +
        (matchMethod.matches() ? 1 : 0);
    if (matchCount == 0) {
      setResult(1, "Failed to parse signature: " + signature, null);
      return;
    } else if (matchCount > 1) {
      setResult(1, "Ambiguous signature: " + signature, null);
    }

    if (matchClass.matches()) {
      String className = DexMember.dexToJavaType(signature);
      lookupClass(className);
      return;
    } else if (matchField.matches()) {
      DexField dexField = new DexField(
          matchField.group(1), matchField.group(2), matchField.group(3));
      Class<?> clazz = Class.forName(dexField.getJavaClassName());
      Field field = clazz.getDeclaredField(dexField.getName());
      setResult(0, "Got field " + field, null);
    } else if (matchMethod.matches()) {
      DexMethod dexMethod = new DexMethod(
          matchMethod.group(1),matchMethod.group(2), matchMethod.group(3));
      Class<?> clazz = Class.forName(dexMethod.getJavaClassName());
      Executable method;
      if (dexMethod.isConstructor()) {
        method = clazz.getConstructor(dexMethod.getParameterTypes());
      } else {
        method = clazz.getMethod(dexMethod.getName(), dexMethod.getParameterTypes());
      }
      setResult(0, "Got method " + method, null);
    }

  }

  @Override
  public void onReceive(Context context, Intent intent) {
    try {
      String className = intent.getStringExtra(EXTRA_CLASS);
      if (className != null) {
        lookupClass(className);
        return;
      }
      String signature = intent.getStringExtra(EXTRA_SIGNATURE);
      if (signature != null) {
        lookupSignature(signature);
        return;
      }

      setResult(1, "No action specified", null);
    } catch (Exception e) {
      StringWriter stack = new StringWriter();
      e.printStackTrace(new PrintWriter(stack));
      setResult(1, stack.toString(), null);
    }
  }

}