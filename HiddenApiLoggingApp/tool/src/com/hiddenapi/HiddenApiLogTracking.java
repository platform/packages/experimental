/*
 * Copyright (C) 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hiddenapi;

import com.android.internal.os.StatsdConfigProto.AtomMatcher;
import com.android.internal.os.StatsdConfigProto.EventMetric;
import com.android.internal.os.StatsdConfigProto.SimpleAtomMatcher;
import com.android.internal.os.StatsdConfigProto.StatsdConfig;
import com.android.os.AtomsProto.Atom;
import com.android.os.StatsLog.ConfigMetricsReportList;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Base64;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

// This tool represents an approximation of what a server would be doing
// - except the directionality of the flow of data is reversed at times;
// The device should send its data directly to the server when its internal
// buffers are full or when otherwise convenient (e.g. connecting to Wi-fi)
public class HiddenApiLogTracking {
  private static String runOnDevice(String cmd) {
    String[] finalCommand = ("adb shell " + cmd).split(" ");
    System.out.println(String.join(" ", finalCommand));
    ProcessBuilder pb = new ProcessBuilder(finalCommand);
    Process process;
    try {
      process = pb.start();
    } catch (IOException e) {
      throw new RuntimeException("Could not start new process.", e);
    }
    StringBuilder output = new StringBuilder();
    try (BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
      for (String line = br.readLine(); line != null; line = br.readLine()) {
        output.append(line).append('\n');
      }
      process.waitFor();
      return output.toString();
    } catch (Exception e) {
      process.destroy();
      throw new RuntimeException("Failed to read command output", e);
    }
  }

  private static String createConfig() {
    StatsdConfig.Builder builder = StatsdConfig.newBuilder().setId(54321);
    final String atomName = "Atom" + System.nanoTime();
    final String eventName = "Event" + System.nanoTime();

    SimpleAtomMatcher.Builder sam =
        SimpleAtomMatcher.newBuilder().setAtomId(Atom.HIDDEN_API_USED_FIELD_NUMBER);
    builder.addAtomMatcher(
        AtomMatcher.newBuilder().setId(atomName.hashCode()).setSimpleAtomMatcher(sam));
    builder.addEventMetric(
        EventMetric.newBuilder().setId(eventName.hashCode()).setWhat(atomName.hashCode()));
    builder.addAllowedLogSource("AID_STATSD");
    return Base64.getEncoder().encodeToString(builder.build().toByteArray());
  }

  private static void processData(String dataAsString) {
    dataAsString = dataAsString.substring(1, dataAsString.length() - 1);
    byte[] data = Base64.getDecoder().decode(dataAsString);
    try {
      ConfigMetricsReportList cmrl = ConfigMetricsReportList.parser().parseFrom(data);
      String output =
          cmrl.getReportsList().stream()
              .flatMap(report -> report.getMetricsList().stream())
              .flatMap(metric -> metric.getEventMetrics().getDataList().stream())
              .map(eventMetric -> eventMetric.getAtom())
              .map(atom -> atom.getHiddenApiUsed().getSignature())
              .collect(Collectors.joining("\n"));
      System.out.println("Used hidden apis");
      System.out.println(output);
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw new RuntimeException("Cannot parse protocol buffer.", e);
    }
  }

  public static void main(String[] args) {
    System.out.println("Setting rate to max rate");
    runOnDevice(
        "am broadcast -n com.android.hiddenapiapp/.Receiver "
            + "-a com.android.hiddenapiapp.SET_RATE --ei rate 65535");
    System.out.println("Sending config");
    String config = createConfig();
    runOnDevice(
        "am broadcast -n com.android.hiddenapiapp/.Receiver "
            + "-a com.android.hiddenapiapp.SET_CONFIG --es config "
            + config);

    System.out.println("Waiting for hidden api accesses... Press enter to read logs");

    try {
      System.in.read();
    } catch (IOException e) {
      System.err.println("There was an IO error");
      e.printStackTrace(System.err);
      System.err.println("Proceeding normally...");
    }

    String output =
        runOnDevice(
            "am broadcast -n com.android.hiddenapiapp/.Receiver "
                + "-a com.android.hiddenapiapp.GET_DATA");
    Pattern p = Pattern.compile("\"([^\"]+)\"");
    Matcher m = p.matcher(output);
    if (m.find()) {
      processData(m.group(0));
    } else {
      System.out.println("No config received!");
    }

    runOnDevice(
        "am broadcast -n com.android.hiddenapiapp/.Receiver "
            + "-a com.android.hiddenapiapp.REMOVE_CONFIG");
  }
}
