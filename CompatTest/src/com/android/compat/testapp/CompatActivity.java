/*
 * Copyright (C) 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.android.compat.testapp;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;

public class CompatActivity extends Activity {

    private static final String TAG = "CompatTest";

    private TextView mText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        mText = findViewById(R.id.text);
        findViewById(R.id.button1).setOnClickListener(view -> {
            try {
                message("Returned: " + accessHiddenApi());
            } catch (ReflectiveOperationException roe) {
                message(roe);
            }
        });
    }

    private Object accessHiddenApi() throws ReflectiveOperationException {
        // Note: this class does not exist. Patch in http://aosp/1008615 in order
        // for this to work as intended.
        Class<?> c = Class.forName("android.compattest.CompatTest");
        Method m = c.getDeclaredMethod("testApi");
        return m.invoke(null);
    }

    private void message(Throwable t) {
        StringWriter s = new StringWriter();
        t.printStackTrace(new PrintWriter(s));
        message(s.toString());
    }

    private void message(String m) {
        Log.d(TAG, m);
        mText.setText(m);
    }
}